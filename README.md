[![DOI](https://zenodo.org/badge/doi/10.5281/zenodo.4686962.svg)](https://doi.org/10.5281/zenodo.4686962)

# Regional SH and HW energy demand for EU28

In this repository, the regional useful energy demand for space heating and hot water for each MS is presented. Additionally, the supply mix structure is divided in six energy carriers: district heating, biomass, natural gas, electricity, other reneables, and other fossil fuels.

## Repository structure
 
Files:
 
```
 
datapackage.json                         -- Datapackage JSON file with the main meta-data
data/Hotmaps_regional_demand.csv         -- Tabular heating technologies and indicators
```
 
## Documentation

The methodology to calculate the deployed energy sources on regional level have been developed in the ESPON project by TU Vienna and Fraunhofer ISI and further refined in Hotmaps [1]. 
The methodological approached combines top-down national energy consumption data with structural data on regional level and bottom-up modelling and simulation of buildings’ energy demand [2].

As first step, available data are on NUTS 3 level are collected and missing data are broken from national level. 
Thereby correlations between structural data such as population, floor space or income and energy consumption patterns as well as other relevant structural data needed for bottom-up modelling approach such as the regional structure of building types and vintages are derived.

In the second step, a bottom-up model is applied to calculate final energy demand of SH and hot water based on technology and building stock data.
The building stock model Invert/EE-Lab is used for the bottom-up simulation . It has been applied in many European and nation projects to analyse building related energy consumption patterns, related RES potentials and scenarios.
The basic idea of the model is to describe the residential and non-residential building stock and the heating, cooling and hot water systems on highly disaggregated level, calculate related energy needs and delivered energy and determine reinvestment cycles and new investment of building components.
The model Invert/EE-Lab up to now has been applied in all countries of EU-28 (+ CH, IS, NO). A representation of the implemented data of the building stock is presented at  [3] . 
A detailed description of the methodology can be found at: [4] and in Müller 2015 [5] or Steinbach 2015 [6]

For detailed explanations and a graphical illustration of the dataset please see the [Hotmaps WP2 report](http://www.hotmaps-project.eu/wp-content/uploads/2018/05/D2.3-Hotmaps_FINAL-VERSION_for-upload.pdf) section 2.5.1 page 93ff.


### References
[1]	C. Schremmer et al., “Territories and low-carbon economy (ESPON Locate),” Luxembourg, 2017  
[2]	L. G. Swan and V. I. Ugursal, “Modeling of end-use energy consumption in the residential sector: A review of modeling techniques,” Renew. Sustain. Energy Rev., vol. 13, pp. 1819–1835, 2009  
[3]	[ENTRANZE] (http://www.entranze.eu/)  
[4]	Vienna University of Technology, “Invert/EE-Lab,” 2015 [Invert/EE-Lab](http://www.invert.at/)  
[5]	A. Müller, “Energy Demand Assessment for Space Conditioning and Domestic Hot Water: A Case Study for the Austrian Building Stock,” Technische Universität Wien, 2015  
[6]	J. Steinbach, Modellbasierte Untersuchung von Politikinstrumenten zur Förderung erneuerbarer Energien und Energieeffizienz im Gebäudebereich. Stuttgart: Fraunhofer Verlag, 2015  


## How to cite

Simon Pezzutto, Stefano Zambotti, Silvia Croce, Pietro Zambelli, Giulia Garegnani, Chiara Scaramuzzino, Ramón Pascual Pascuas, Alyona Zubaryeva, Franziska Haas, Dagmar Exner (EURAC), Andreas Müller (e‐think), Michael Hartner (TUW), Tobias Fleiter, Anna‐Lena Klingler, Matthias Kühnbach, Pia Manz, Simon Marwitz, Matthias Rehfeldt, Jan Steinbach, Eftim Popovski (Fraunhofer ISI) Reviewed by Lukas Kranzl, Sara Fritz (TUW)
Hotmaps Project, D2.3 WP2 Report – Open Data Set for the EU28, 2018 [www.hotmaps-project.eu](http://www.hotmaps-project.eu/wp-content/uploads/2018/05/D2.3-Hotmaps_FINAL-VERSION_for-upload.pdf) 


## Authors
Eftim Popovski <sup>*</sup>,

<sup>*</sup> [Fraunhofer ISI](https://www.isi.fraunhofer.de/en.html)  
Fraunhofer Institute for Systems and Innovation Research  
Breslauer Strasse 48  
76139 Karlsruhe  


## License
Copyright © 2016 - 2020, Eftim Popovski

SPDX-License-Identifier: CDLA-Permissive-1.0

License-Text: [Community Data License Agreement Permissive 1.0](https://cdla.io/permissive-1-0/)

This work is licensed under a Community Data License Agreement – Permissive – Version 1.0

## Acknowledgement
We would like to convey our deepest appreciation to the Horizon 2020 [Hotmaps Project](http://www.hotmaps-project.eu/) (Grant Agreement number 723677), which provided the funding to carry out the present investigation.

